# -*- coding: utf-8 -*-
from django.views.generic import ListView
from datetime import date
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from models import *
import locale
# Create your views here.
def index(request):
    return render(request, 'NoteManager/index.html')

def new(request):
    return render(request, 'NoteManager/new.html', { 'categories': Category.objects.all() })

def create(request):
    pass

def show(request, note_id):
    note = Note.objects.get(pk = note_id)
    return render(request, 'NoteManager/show.html', { 'note': note })

def edit(request, note_id):
    note = Note.objects.get(pk = note_id)
    categories = Category.objects.all()
    context = { 'note': note, 'categories': categories }
    return render(request, 'NoteManager/edit.html', context)

def create(request):
    user = request.user
    if user.is_authenticated():
        note = user.note_set.create()
        note.title = request.POST['title']
        note.content = request.POST['content']
        note.category = Category.objects.get(name = request.POST['category'])
        if 'chosen' in request.POST:
            note.chosen = True
        else:
            note.chosen = False

        try:
            note.save()
            return HttpResponseRedirect('/note/%d' % note.id)
        except:
            return render(request, 'NoteManager/new.html', { 'note': note })

def delete(request, note_id):
    if request.user.is_authenticated():
        note = Note.objects.get(pk = note_id)
        if note in request.user.note_set.all():
            note.delete()
    return HttpResponseRedirect('/notes')

def ajax_delete(request, note_id):
    if request.is_ajax() and request.user.is_authenticated():
        try:
            note = Note.objects.get(pk = note_id)
            if note in request.user.note_set.all():
                note.delete()
            else:
                HttpResponseRedirect('/notes')
        except:
            HttpResponseRedirect('/notes')
    else:
        HttpResponseRedirect('/notes')
    return HttpResponse('')

def update(request, note_id):
    note = Note.objects.get(pk = note_id)
    user = request.user
    if user.is_authenticated():
        if note in user.note_set.all():
            note.title = request.POST['title']
            note.content = request.POST['content']
            note.category = Category.objects.get(name = request.POST['category'])
            if 'chosen' in request.POST:
                note.chosen = True
            else:
                note.chosen = False
            try:
                note.save()
                request.session['notice'] = 'Note was successfuly updated.'
                return HttpResponseRedirect('/note/%d' % int(note_id))
            except Exception, e:
                request.session['notice'] = e.message
                return HttpResponseRedirect('/note/%d/edit' % int(note_id))

    return render(request, 'NoteManager/signin.html')

def createUser(request):
    if request.method == 'POST': # If the form has been submitted...
        form = UserCreationForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = User.objects.create_user(username = username, password = password)
            user.save
            return HttpResponseRedirect('/notes') # Redirect after POST
    else:
        form = UserCreationForm() # An unbound form

    return render(request, 'NoteManager/signup.html', {
        'form': form,
    })

def notes(request):
    user = request.user
    if user.is_authenticated():
        if 'category' in request.POST:
            category = request.POST['category']
        else:
            category = 'All categories'

        if 'keyword' in request.POST:
            keyword = request.POST['keyword']
        else:
            keyword = ''

        chosen = 'chosen' in request.POST

        if 'sort' in request.POST:
            sort = request.POST['sort']
        else:
            sort = 'DESC'

        notes = user.note_set.filter(title__icontains = keyword)

        if chosen:
            notes = notes.filter(chosen = True)

        if category != 'All categories':
            notes = notes.filter(category = Category.objects.get(name = category))

        if sort == 'DESC':
            notes = notes.order_by('-created_at')
        elif sort == 'ASC':
            notes = notes.order_by('created_at')
        elif sort == 'chosen':
            notes = notes.order_by('-chosen')
        elif sort == 'category':
            notes == notes.order_by('category_id')

        context = { 'notes': notes,
                    'user': user,
                    'categories': Category.objects.all(),
                    'category': category,
                    'sort': sort,
                    'keyword': keyword,
                    'chosen': chosen
        }
        return render(request, 'NoteManager/notes.html', context)
    else:
        return render(request, 'NoteManager/signin.html')

def signin(request):
    if not request.user.is_authenticated():
        return render(request, 'NoteManager/signin.html')
    else:
        return HttpResponseRedirect('/notes')

def signup(request):
    if not request.user.is_authenticated():
        return render(request, 'NoteManager/signup.html')
    else:
        return HttpResponseRedirect('/notes')

def auth(request):
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/notes')
        else:
            return render(request, 'NoteManager/signin.html', { 'errors': 'An error occured' })
    else:
        return render(request, 'NoteManager/signin.html')

def signout(request):
    logout(request)
    return HttpResponseRedirect('/')

def ajax_notes(request):
    user = request.user
    if user.is_authenticated():
        try:
            if 'category' in request.POST:
                category = request.POST['category']
            else:
                category = 'All categories'

            if 'keyword' in request.POST:
                keyword = request.POST['keyword']
            else:
                keyword = ''

            chosen = 'chosen' in request.POST

            if 'sort' in request.POST:
                sort = request.POST['sort']
            else:
                sort = 'DESC'

            notes = user.note_set.filter(title__icontains = keyword)

            if chosen:
                notes = notes.filter(chosen = True)

            if category != 'All categories':
                notes = notes.filter(category = Category.objects.get(name = category))

            if sort == 'DESC':
                notes = notes.order_by('-created_at')
            elif sort == 'ASC':
                notes = notes.order_by('created_at')
            elif sort == 'chosen':
                notes = notes.order_by('-chosen')
            elif sort == 'category':
                notes == notes.order_by('category_id')
            msg = ''
            for note in notes:
                msg = msg + '''<p><a href="/note/%d">%s</a> <small>%s</small></p>
                        <p>%s</p>
                        <p>
                            <a href="/note/%d/edit">Edit note</a> |
                            <a href="/note/%d/delete">Delete note</a>
                        </p>
                        <hr>
                      ''' % (note.id, note.title, note.created_at.strftime('%d %B %Y %H:%M:%S'), note.content, note.id, note.id)
        except Exception, e:
            msg = e.message
    return HttpResponse(msg)