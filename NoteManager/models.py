# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django import forms
from django.utils import timezone
# Create your models here.

class Category(models.Model):
    # дата создания
    created_at = models.DateTimeField()
    # название категории
    name = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['id']

class Note(models.Model):
    # дата создания
    created_at = models.DateTimeField()
    # заголовок
    title = models.CharField(max_length = 255)
    # содержимое
    content = models.TextField(max_length = 2500)
    # категория
    category = models.ForeignKey('Category')
    # автор
    user = models.ForeignKey(User)
    # флажок
    chosen = models.BooleanField(default = False)

    def __unicode__(self):
        return self.title

    def save(self, force_insert = False, force_update = False, using = None):
        if not self.created_at:
            self.created_at = timezone.now()
        super(Note, self).save()