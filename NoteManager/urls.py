# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from NoteManager import views

urlpatterns = patterns('',
    # home
    url(r'^$', views.index, name = 'index'),
    # юзеры
    url(r'^signup', views.signup, name = 'signup'),
    url(r'^signin', views.signin, name = 'signin'),
    url(r'^create-user', views.createUser, name = 'create-user'),
    url(r'^auth$', views.auth),
    url(r'^signout$', views.signout, name = 'signout'),
    # заметки
    url(r'^notes$', views.notes),
    url(r'^note/(?P<note_id>\d+)/$', views.show, name = 'show-note'),
    url(r'^note/(?P<note_id>\d+)/edit$', views.edit, name = 'edit-note'),
    url(r'^note/(?P<note_id>\d+)/update$', views.update, name = 'update-note'),
    url(r'^note/(?P<note_id>\d+)/delete$', views.delete, name = 'delete-note'),
    url(r'^note/(?P<note_id>\d+)/ajax_delete$', views.ajax_delete),
    url(r'^note/new$', views.new, name = 'new-note'),
    url(r'^note/create$', views.create, name = 'create-note'),
    url(r'^ajax_notes$', views.ajax_notes)
)